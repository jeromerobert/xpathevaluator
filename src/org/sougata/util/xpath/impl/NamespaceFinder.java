/* * XPath Evaluator plugin for Netbeans
 * 
 * Copyright (C) 2009,  Sougata Ghosh
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU Lesser General Public License as published by 
 * the Free Software Foundation; either version 2.1 of the License, or 
 * (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public 
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library.
 */
package org.sougata.util.xpath.impl;

import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

/**
 *
 * @author Sougata
 */
public class NamespaceFinder {

    public Map<String, String> getNamespaceMappings(InputSource is) throws Exception {
        Map<String, String> nsMapping = new HashMap<String, String>();
        try {
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = db.parse(is);
            Node root = doc.getFirstChild();
            while (root.getNodeType() != Node.ELEMENT_NODE) {
                root = root.getNextSibling();
            }
            NamedNodeMap atts = root.getAttributes();
            if (atts == null || atts.getLength() == 0) {
                return null;
            }
            for (int i = 0; i < atts.getLength(); i++) {
                Node att = atts.item(i);
                if (att.getNodeName().startsWith("xmlns:")) {
                    //System.out.println(att.getNodeName().substring(6) + "=" + att.getNodeValue());
                    nsMapping.put(att.getNodeName().substring(6), att.getNodeValue());
                }
            }
        } catch (Exception e) {
            //e.printStackTrace();
            throw e;
        }
        return nsMapping;
    }
}
