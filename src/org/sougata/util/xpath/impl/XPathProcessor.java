/* * XPath Evaluator plugin for Netbeans
 * 
 * Copyright (C) 2009,  Sougata Ghosh
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU Lesser General Public License as published by 
 * the Free Software Foundation; either version 2.1 of the License, or 
 * (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public 
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library.
 */
package org.sougata.util.xpath.impl;

import java.util.Iterator;
import java.util.Map;
import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.sougata.util.misc.StringUtil;
import org.xml.sax.InputSource;

/**
 *
 * @author Sougata
 */
public class XPathProcessor {

    public Object evaluate(InputSource is, String xpath, Map nsMap, QName qName) throws XPathExpressionException {
        System.out.println("XPathProcessor#evaluate:" + xpath + "\n" + nsMap + "\n" + qName);
        Object result = null;
        try {
            XPathFactory xpf = XPathFactory.newInstance();
            XPath path = xpf.newXPath();
            if (nsMap != null && !nsMap.isEmpty()) {
                path.setNamespaceContext(new NamespaceResolver(nsMap));
            }
            XPathExpression expr = path.compile(xpath);
            result = expr.evaluate(is, qName);
        } catch (XPathExpressionException ex) {
            ex.printStackTrace();
            throw ex;
        }
        System.out.println("XPathProcessor#evaluate:" + result);
        return result;
    }

    private static class NamespaceResolver implements NamespaceContext {

        Map namespaceContext = null;

        public NamespaceResolver(Map namespaceContext) {
            this.namespaceContext = namespaceContext;
        }

        public String getNamespaceURI(String prefix) {

            if (StringUtil.isEmptyString(prefix)) {
                return null;
            }
            String uri = null;
            if (this.namespaceContext != null && this.namespaceContext.containsKey(prefix)) {
                uri = (String) this.namespaceContext.get(prefix);
            }
            return uri;
        }

        // Dummy implementation - not used!
        public Iterator getPrefixes(String val) {
            return null;
        }

        // Dummy implemenation - not used!
        public String getPrefix(String uri) {
            return null;
        }
    }
}
