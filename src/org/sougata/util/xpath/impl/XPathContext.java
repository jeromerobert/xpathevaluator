/* * XPath Evaluator plugin for Netbeans
 * 
 * Copyright (C) 2009,  Sougata Ghosh
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU Lesser General Public License as published by 
 * the Free Software Foundation; either version 2.1 of the License, or 
 * (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public 
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library.
 */
package org.sougata.util.xpath.impl;

import java.util.Map;

/**
 * 
 * @author Sougata
 */
public class XPathContext {

    private Map namespaces = null;

    /**
     * @return the namespaces
     */
    public Map getNamespaces() {
        return namespaces;
    }

    /**
     * @param namespaces
     *            the namespaces to set
     */
    public void setNamespaces(Map namespaces) {
        this.namespaces = namespaces;
    }
}
