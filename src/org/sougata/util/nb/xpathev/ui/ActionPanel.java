/* * XPath Evaluator plugin for Netbeans
 * 
 * Copyright (C) 2009,  Sougata Ghosh
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU Lesser General Public License as published by 
 * the Free Software Foundation; either version 2.1 of the License, or 
 * (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public 
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library.
 */
package org.sougata.util.nb.xpathev.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.MouseListener;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author Sougata
 */
public class ActionPanel extends JPanel {

    private String text;
    private static Font font;
    private boolean selected;
    private BufferedImage open,  closed;
    private Rectangle target;
    final int OFFSET = 30,  PAD = 5;
    private static ImageIcon g_plus = null;
    private static ImageIcon g_minus = null;
    

    static {
        g_plus = new ImageIcon(ActionPanel.class.getResource("g_plus.gif"));
        g_minus = new ImageIcon(ActionPanel.class.getResource("g_minus.gif"));
        font = new Font("Tahoma", Font.PLAIN, 11);
    }

    public ActionPanel(String text, MouseListener ml) {
        this.text = text;
        addMouseListener(ml);
        
        selected = false;
        //setBackground(new Color(200, 200, 220));
        setPreferredSize(new Dimension(200, 20));
        //setBorder(BorderFactory.createRaisedBevelBorder());
        //setBorder(BorderFactory.createEmptyBorder());
        //setBorder(BorderFactory.createEtchedBorder());
        //setPreferredSize(new Dimension(200, 20));
        createImages();
        setRequestFocusEnabled(true);
    }

    public void toggleSelection() {
        selected = !selected;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        int w = getWidth();
        int h = getHeight();
        /**
        if (selected) {
        g2.drawImage(open, PAD, 0, this);
        } else {
        g2.drawImage(closed, PAD, 0, this);
        }
         **/
        if (selected) {
            g2.drawImage(g_plus.getImage(), PAD, 3, this);
        } else {
            g2.drawImage(g_minus.getImage(), PAD, 3, this);
        }

        g2.setFont(font);
        FontRenderContext frc = g2.getFontRenderContext();
        LineMetrics lm = font.getLineMetrics(text, frc);
        float height = lm.getAscent() + lm.getDescent();
        float x = OFFSET;
        float y = (h + height) / 2 - lm.getDescent();
        g2.drawString(text, x, y);
    }

    private void createImages() {
        int w = 20;
        int h = getPreferredSize().height;
        target = new Rectangle(2, 0, 20, 18);
        open = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2 = open.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setPaint(getBackground());
        g2.fillRect(0, 0, w, h);
        int[] x = {2, w / 2, 18};
        int[] y = {4, 15, 4};
        Polygon p = new Polygon(x, y, 3);
        g2.setPaint(Color.GRAY);
        g2.fill(p);
        //g2.setPaint(Color.DARK_GRAY.brighter());
        g2.draw(p);
        g2.dispose();
        closed = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        g2 = closed.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setPaint(getBackground());
        g2.fillRect(0, 0, w, h);
        x = new int[]{3, 13, 3};
        y = new int[]{4, h / 2, 16};
        p = new Polygon(x, y, 3);
        g2.setPaint(Color.GRAY);
        g2.fill(p);
        //g2.setPaint(Color.DARK_GRAY.brighter());
        g2.draw(p);
        g2.dispose();
    }

    public Rectangle getTarget() {
        return target;
    }
}
