/* * XPath Evaluator plugin for Netbeans
 * 
 * Copyright (C) 2009,  Sougata Ghosh
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU Lesser General Public License as published by 
 * the Free Software Foundation; either version 2.1 of the License, or 
 * (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public 
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library.
 */
package org.sougata.util.nb.xpathev.impl;

import java.io.BufferedInputStream;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.Map;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import org.openide.filesystems.FileObject;
import org.sougata.util.xpath.impl.NamespaceFinder;
import org.sougata.util.xpath.impl.XPathProcessor;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 *
 * @author Sougata
 */
public class XPathActionImpl {

//    public InputSource getXMLSource() throws Exception {
//        InputSource is = null;
//        try {
//            JTextComponent j = Utilities.getFocusedComponent();
//            if (j == null) {
//                j = Utilities.getLastActiveComponent();
//            }
//            is = new InputSource(new BufferedReader(new StringReader(j.getText())));
//        } catch (Exception e) {
//            //e.printStackTrace();
//            throw e;
//        }
//        return is;
//    }
    public Map<String, String> getNsMappings(InputSource is) throws Exception {
        Map<String, String> map = null;
        try {
            NamespaceFinder nf = new NamespaceFinder();
            map = nf.getNamespaceMappings(is);

        } catch (Exception e) {
            //e.printStackTrace();
            throw e;
        }
        return map;
    }

    public XPathResult evaluate(XPathEditor xpe, FileObject fo) throws XPathExpressionException, Exception {
        XPathResult xpResult = new XPathResult();
        XPathProcessor xpp = new XPathProcessor();
        BufferedInputStream bf = null;
        try {
            bf = new BufferedInputStream(fo.getInputStream());
            InputSource is = new InputSource(bf);
            Object result = null;
            if (xpe.getNsDataType() == XPathConstants.NODESET) {
                result = xpp.evaluate(is, xpe.getXpath(),
                        xpe.getNsMap(), XPathConstants.NODESET);
            } else if (xpe.getNsDataType() == XPathConstants.NODE) {
                result = xpp.evaluate(is, xpe.getXpath(),
                        xpe.getNsMap(), XPathConstants.NODE);
            } else if (xpe.getNsDataType() == XPathConstants.STRING) {
                result = xpp.evaluate(is, xpe.getXpath(),
                        xpe.getNsMap(), XPathConstants.STRING);
            } else if (xpe.getNsDataType() == XPathConstants.NUMBER) {
                result = xpp.evaluate(is, xpe.getXpath(),
                        xpe.getNsMap(), XPathConstants.NUMBER);
            } else if (xpe.getNsDataType() == XPathConstants.BOOLEAN) {
                result = xpp.evaluate(is, xpe.getXpath(),
                        xpe.getNsMap(), XPathConstants.BOOLEAN);
            }
            xpResult.setResult(getResult(result));
            xpResult.setXpath(xpe.getXpath());
        } catch (XPathExpressionException e) {
            xpResult.setException(e);
            //e.printStackTrace();
            throw e;
        } catch (Exception e) {
            xpResult.setException(e);
            //e.printStackTrace();
            throw e;
        } finally {
            if (bf != null) {
                bf.close();
            }
        }
        return xpResult;
    }

    private StringBuffer getResult(Object result) throws XPathExpressionException, Exception {
        StringBuffer sBuf = new StringBuffer();
        if (result instanceof Double) {
            sBuf.append("" + result); //$NON-NLS-1$
        } else if (result instanceof Boolean) {
            sBuf.append("" + ((Boolean) result).booleanValue()); //$NON-NLS-1$
        } else if (result instanceof String) {
            sBuf.append("" + result); //$NON-NLS-1$
        } else if (result instanceof Node) {
            Node node = (Node) result;
            getNode(node, sBuf);
        } else if (result instanceof NodeList) {
            NodeList nodelist = (NodeList) result;
            getNodeList(nodelist, sBuf);
        }
        return sBuf;
    }

    private boolean isTextNode(Node n) {
        if (n == null) {
            return false;
        }
        short nodeType = n.getNodeType();
        return nodeType == Node.CDATA_SECTION_NODE || nodeType == Node.TEXT_NODE;
    }

    private void getNode(Node node, StringBuffer sBuf) throws Exception {
        if (isTextNode(node)) {
            sBuf.append(node.getNodeValue());
        } else {
            // Set up an identity transformer to use as serializer.
            Transformer serializer = TransformerFactory.newInstance().newTransformer();
            serializer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes"); //$NON-NLS-1$
            //serializer.setOutputProperty(OutputKeys.INDENT, "yes"); //$NON-NLS-1$
            //serializer.setOutputProperty(OutputKeys.METHOD, "xml"); //$NON-NLS-1$
            StringWriter sWriter = new StringWriter();
            serializer.transform(new DOMSource(node), new StreamResult(sWriter));
            sBuf.append(sWriter.toString());
        }

    }

    private void getNodeList(NodeList nodelist, StringBuffer sBuf) throws Exception {
        Node n;
        Transformer serializer = TransformerFactory.newInstance().newTransformer();
        serializer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes"); //$NON-NLS-1$
        //serializer.setOutputProperty(OutputKeys.INDENT, "yes"); //$NON-NLS-1$
        //serializer.setOutputProperty(OutputKeys.METHOD, "xml"); //$NON-NLS-1$
        for (int i = 0; i < nodelist.getLength(); i++) {
            n = nodelist.item(i);
            if (isTextNode(n)) {
                StringBuffer sb = new StringBuffer(n.getNodeValue());
                for (Node nn = n.getNextSibling(); isTextNode(nn); nn = nn.getNextSibling()) {
                    sb.append(nn.getNodeValue() + "\r\n"); //$NON-NLS-1$
                }
                sBuf.append(sb);
            } else {
                StringWriter sWriter = new StringWriter();
                serializer.transform(new DOMSource(n), new StreamResult(sWriter));
                sBuf.append(sWriter.toString() + "\r\n"); //$NON-NLS-1$
            }
        }
    }

    public void updateNsTable(JTable jTable, boolean checked, Map nsMap) {
        TableModel tm = jTable.getModel();
        DefaultTableModel model = (DefaultTableModel) tm;
        this.clearNsTable(jTable);
        if (nsMap != null && !nsMap.isEmpty()) {
            Iterator it = nsMap.keySet().iterator();
            while (it.hasNext()) {
                Object key = it.next();
                Object value = nsMap.get(key);
                model.addRow(new Object[]{key, value});
            }
        }
    }

    public void clearNsTable(JTable jTable) {
        TableModel tm = jTable.getModel();
        DefaultTableModel model = (DefaultTableModel) tm;
        if (model != null) {
            for (int i = model.getRowCount() - 1; i >= 0; i--) {
                model.removeRow(i);
                jTable.revalidate();
            }
        }
    }
}
