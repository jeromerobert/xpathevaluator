/* * XPath Evaluator plugin for Netbeans
 * 
 * Copyright (C) 2009,  Sougata Ghosh
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU Lesser General Public License as published by 
 * the Free Software Foundation; either version 2.1 of the License, or 
 * (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public 
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library.
 */
package org.sougata.util.nb.xpathev.impl;

import java.util.Map;
import javax.xml.namespace.QName;

/**
 *
 * @author Sougata
 */
public class XPathEditor {

    private String xpath = null;
    private QName nsDataType = null;
    private Map nsMap = null;

    public String getXpath() {
        return xpath;
    }

    public void setXpath(String xpath) {
        this.xpath = xpath;
    }

    public QName getNsDataType() {
        return nsDataType;
    }

    public void setNsDataType(QName nsDataType) {
        this.nsDataType = nsDataType;
    }

    public Map getNsMap() {
        return nsMap;
    }

    public void setNsMap(Map nsMap) {
        this.nsMap = nsMap;
    }
}
