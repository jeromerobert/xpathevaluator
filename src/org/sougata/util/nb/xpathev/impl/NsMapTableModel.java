/* * XPath Evaluator plugin for Netbeans
 * 
 * Copyright (C) 2009,  Sougata Ghosh
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU Lesser General Public License as published by 
 * the Free Software Foundation; either version 2.1 of the License, or 
 * (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public 
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library.
 */

package org.sougata.util.nb.xpathev.impl;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Sougata
 */
public class NsMapTableModel extends DefaultTableModel {

    public NsMapTableModel() {
        super(new Object[][]{},
                new String[]{
                    "Prefix", "URI"
                });
    }
    Class[] types = new Class[]{
        java.lang.String.class, java.lang.String.class
    };
    boolean[] canEdit = new boolean[]{
        false, false
    };

    public Class getColumnClass(int columnIndex) {
        return types[columnIndex];
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return canEdit[columnIndex];
    }
}
