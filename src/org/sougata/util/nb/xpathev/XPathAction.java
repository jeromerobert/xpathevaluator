/* * XPath Evaluator plugin for Netbeans
 * 
 * Copyright (C) 2009,  Sougata Ghosh
 * 
 * This library is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU Lesser General Public License as published by 
 * the Free Software Foundation; either version 2.1 of the License, or 
 * (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public 
 * License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library.
 */
package org.sougata.util.nb.xpathev;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.util.Utilities;
import org.openide.windows.TopComponent;

/**
 * Action which shows XPath component.
 */
public class XPathAction extends AbstractAction {

    public XPathAction() {
        super(NbBundle.getMessage(XPathAction.class, "CTL_XPathAction"));
        putValue(SMALL_ICON, new ImageIcon(ImageUtilities.loadImage(XPathTopComponent.ICON_PATH, true)));
    }

    public void actionPerformed(ActionEvent evt) {
        TopComponent win = XPathTopComponent.findInstance();
        win.open();
        win.requestActive();
    }
}
